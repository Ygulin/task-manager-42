package ru.tsc.gulin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "command";

    @NotNull
    public static final String DESCRIPTION = "Show application commands";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
