package ru.tsc.gulin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.request.DataYamlFasterXmlLoadRequest;
import ru.tsc.gulin.tm.dto.request.UserLogoutRequest;
import ru.tsc.gulin.tm.enumerated.Role;

public final class DataYamlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD DATA FROM YAML FILE]");
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlFasterXmlLoadRequest(getToken()));
        serviceLocator.getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
