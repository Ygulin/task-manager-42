package ru.tsc.gulin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.request.DataXmlFasterXmlSaveRequest;
import ru.tsc.gulin.tm.enumerated.Role;

public final class DataXmlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE DATA IN XML FILE]");
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlFasterXmlSaveRequest(getToken()));
    }

}
