package ru.tsc.gulin.tm.endpoint;

import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    /* @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @Nullable
    private String token;

    @Nullable
    private Project initProject;

    @Before
    public void init() {
        token = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        initProject =
                projectEndpoint.createProject(new ProjectCreateRequest(token, "init project", "init")).getProject();
    }

    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest("", "name", "desc"))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(token, "", "desc"))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(token, "name", ""))
        );
        @NotNull final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(token, "name", "desc"));
        Assert.assertNotNull(response);
        Assert.assertEquals("name", response.getProject().getName());
    }

    @Test
    public void listProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest("", null)));
        @NotNull final ProjectListResponse response = projectEndpoint.listProject(new ProjectListRequest(token, null));
        Assert.assertNotNull(response);
        Assert.assertEquals(1, response.getProjects().size());
        @NotNull final ProjectListResponse responseSort =
                projectEndpoint.listProject(new ProjectListRequest(token, Sort.BY_CREATED));
        Assert.assertNotNull(responseSort);
        Assert.assertEquals(1, responseSort.getProjects().size());
    }

    @Test
    public void clearProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(new ProjectClearRequest("")));
        @NotNull final ProjectClearResponse response =
                projectEndpoint.clearProject(new ProjectClearRequest(token));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest("", initProject.getId(), Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, "", Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, initProject.getId(), null)
                )
        );
        @NotNull final ProjectChangeStatusByIdResponse response =
                projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, initProject.getId(), Status.IN_PROGRESS)
                );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest("", 0, Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, -1, Status.IN_PROGRESS)
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, 0, null)
                )
        );
        @NotNull final ProjectChangeStatusByIndexResponse response =
                projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, 0, Status.IN_PROGRESS)
                );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest("", initProject.getId()))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, "")));
        @NotNull final ProjectRemoveByIdResponse response =
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, initProject.getId()));
        Assert.assertNotNull(response);
        Assert.assertNull(projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, initProject.getId())).getProject()
        );
    }

    @Test
    public void removeProjectByIndex() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest("", 0))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, null)));
        @NotNull final ProjectRemoveByIndexResponse response =
                projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, 0));
        Assert.assertNotNull(response);
        Assert.assertNull(projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, initProject.getId())).getProject()
        );
    }

    @Test
    public void showProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest("", initProject.getId()))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(token, ""))
        );
        @NotNull final ProjectShowByIdResponse response =
                projectEndpoint.showProjectById(new ProjectShowByIdRequest(token, initProject.getId()));
        Assert.assertNotNull(response);
        Assert.assertEquals(initProject.getName(), response.getProject().getName());
    }

    @Test
    public void showProjectByIndex() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest("", 0))
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(token, null))
        );
        @NotNull final ProjectShowByIndexResponse response =
                projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(token, 0));
        Assert.assertNotNull(response);
        Assert.assertEquals(initProject.getName(), response.getProject().getName());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest("", initProject.getId(), "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(token, "", "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(token, initProject.getId(), "", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(token, initProject.getId(), "new name", "")
                )
        );
        @NotNull final ProjectUpdateByIdResponse response =
                projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(token, initProject.getId(), "new name", "new desc")
                );
        Assert.assertNotNull(response);
        Assert.assertEquals("new name", response.getProject().getName());
        Assert.assertEquals("new desc", response.getProject().getDescription());
    }

    @Test
    public void updateProjectByIndex() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest("", 0, "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(token, null, "new name", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(token, 0, "", "new desc")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(token, 0, "new name", "")
                )
        );
        @NotNull final ProjectUpdateByIndexResponse response =
                projectEndpoint.updateProjectByIndex(
                        new ProjectUpdateByIndexRequest(token, 0, "new name", "new desc")
                );
        Assert.assertNotNull(response);
        Assert.assertEquals("new name", response.getProject().getName());
        Assert.assertEquals("new desc", response.getProject().getDescription());
    } */

}
