package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@NotNull final SessionDTO model);

    @NotNull
    SessionDTO add(@Nullable final String userId, @NotNull final SessionDTO model);

    void clear(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    int getSize(@Nullable final String userId);

    @Nullable
    List<SessionDTO> findAll(@Nullable final String userId);

    @Nullable
    SessionDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void remove(@Nullable final String userId, @Nullable final SessionDTO model);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

}
