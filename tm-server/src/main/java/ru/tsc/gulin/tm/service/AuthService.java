package ru.tsc.gulin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.IAuthService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.ISessionService;
import ru.tsc.gulin.tm.api.service.IUserService;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.exception.field.LoginEmptyException;
import ru.tsc.gulin.tm.exception.field.PasswordEmptyException;
import ru.tsc.gulin.tm.exception.system.AccessDeniedException;
import ru.tsc.gulin.tm.exception.system.AuthenticationException;
import ru.tsc.gulin.tm.dto.model.SessionDTO;
import ru.tsc.gulin.tm.dto.model.UserDTO;
import ru.tsc.gulin.tm.util.CryptUtil;
import ru.tsc.gulin.tm.util.HashUtil;

import java.util.Date;
import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @Nullable
    @Override
    public UserDTO registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        Optional.ofNullable(token).orElseThrow(AccessDeniedException::new);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (Exception e ) {
            throw new AccessDeniedException(e);
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter(item -> !item.isEmpty()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).filter(item -> !item.isEmpty()).orElseThrow(PasswordEmptyException::new);
        @Nullable final UserDTO user = userService.findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(AuthenticationException::new);
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        Optional.ofNullable(hash).orElseThrow(AuthenticationException::new);
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @Override
    public void logout(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.removeById(session.getUserId(), session.getId());
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }

}
