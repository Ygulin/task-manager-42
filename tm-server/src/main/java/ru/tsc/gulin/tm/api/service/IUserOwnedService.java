package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {
}
