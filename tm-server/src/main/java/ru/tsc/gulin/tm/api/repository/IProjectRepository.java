package ru.tsc.gulin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @Insert("INSERT INTO TM_PROJECT (id, name, description, status, user_id, created_dt, start_dt, end_dt)"
            + " VALUES (#{id}, #{name}, #{description}, #{status}, #{userId}, #{created}, #{dateStart}, #{dateEnd})"
    )
    void add(@NotNull final ProjectDTO project);

    @Insert("INSERT INTO TM_PROJECT (id, name, description, status, user_id, created_dt, start_dt, end_dt)"
            + " VALUES (#{id}, #{name}, #{description}, #{status}, #{userId}, #{created}, #{dateStart}, #{dateEnd})"
    )
    void addAll(@NotNull final Collection<ProjectDTO> projects);

    @Update("UPDATE TM_PROJECT SET name = #{name}, description = #{description}, status = #{status},"
            + " created_dt = #{created}, start_dt = #{dateStart}, end_dt = #{dateEnd} WHERE id = #{id}"
    )
    void update(@NotNull final ProjectDTO project);

    @NotNull
    @Select("SELECT id, name, description, status, user_id, created_dt, start_dt, end_dt FROM TM_PROJECT")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT id, name, description, status, user_id, created_dt, start_dt, end_dt FROM TM_PROJECT"
            + " WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<ProjectDTO> findAllByUserId(@Param("userId") @NotNull final String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<ProjectDTO> findAllSorted(@NotNull final SelectStatementProvider selectStatementProvider);

    @Nullable
    @Select("SELECT id, name, description, status, user_id, created_dt, start_dt, end_dt FROM TM_PROJECT"
            + " WHERE id = #{id}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    ProjectDTO findOneById(@Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT id, name, description, status, user_id, created_dt, start_dt, end_dt FROM TM_PROJECT"
            + " WHERE id = #{id} AND user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateStart", column = "start_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    ProjectDTO findOneByIdAndUserId(@Param("user_id") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_PROJECT WHERE id = #{id}")
    void remove(@NotNull final ProjectDTO project);

    @Delete("DELETE FROM TM_PROJECT WHERE id = #{id} AND user_id = #{user_id}")
    void removeByUserId(@Param("user_id") @NotNull final String userId, @NotNull final ProjectDTO project);

    @Delete("DELETE FROM TM_PROJECT WHERE id = #{id}")
    void removeById(@Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_PROJECT WHERE id = #{id} AND user_id = #{userId}")
    void removeByIdAndUserId(@Param("user_id") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Delete("DELETE FROM TM_PROJECT")
    void clear();

    @Delete("DELETE FROM TM_PROJECT WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull final String userId);

    @Select("SELECT count(1) = 1 FROM TM_PROJECT WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull final String id);

    @Select("SELECT count(1) = 1 FROM TM_PROJECT WHERE id = #{id} AND user_id = #{userId}")
    boolean existsByIdAndUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

    @Select("SELECT count(1) FROM TM_PROJECT")
    long getSize();

    @Select("SELECT count(1) FROM TM_PROJECT WHERE user_id = #{userId}")
    long getSizeByUserId(@Param("userId") @NotNull final String userId);

}
