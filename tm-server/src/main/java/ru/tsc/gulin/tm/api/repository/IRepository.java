package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    @NotNull
    List<M> findAll();

    void add(@NotNull M model);

    Collection<M> set(@NotNull Collection<M> models);

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    @Nullable
    void removeById(@NotNull String id);

    void update(@NotNull M model);

    void clear();

    boolean existsById(String id);

    long getSize();

}
