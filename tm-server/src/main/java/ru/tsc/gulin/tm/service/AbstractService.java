package ru.tsc.gulin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.IRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IService;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;

public abstract class AbstractService<M extends AbstractModelDTO, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull SqlSession getSqlSession() {
        return connectionService.getSqlSession();
    }

}
