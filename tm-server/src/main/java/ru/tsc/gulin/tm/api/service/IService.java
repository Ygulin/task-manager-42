package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IRepository;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

}
