package ru.tsc.gulin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.tsc.gulin.tm.api.repository.IProjectRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IProjectService;
import ru.tsc.gulin.tm.comparator.CreatedComparator;
import ru.tsc.gulin.tm.comparator.DateBeginComparator;
import ru.tsc.gulin.tm.comparator.StatusComparator;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.entity.ModelNotFoundException;
import ru.tsc.gulin.tm.exception.field.*;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.dto.model.ProjectProvider;

import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.select.SelectDSL.select;
import static ru.tsc.gulin.tm.dto.model.ProjectProvider.*;

public class ProjectService extends AbstractUserOwnedService<ProjectDTO, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected SortSpecification getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateStart;
        else return name;
    }

    @NotNull
    public List<ProjectDTO> findAll() {
        try (@NotNull SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        try (@NotNull SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (comparator == null) return findAll();
        try (@NotNull SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final SelectStatementProvider selectStatement =
                    select(id, ProjectProvider.userId, name, description, status, created, dateStart, dateEnd)
                            .from(project)
                            .where(ProjectProvider.userId, isEqualTo(userId))
                            .orderBy(getSortType(comparator))
                            .build()
                            .render(RenderingStrategies.MYBATIS3);
            return repository.findAllSorted(selectStatement);
        }
    }

    @NotNull
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        try (@NotNull SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return findAll(userId, sort.getComparator());
        }
    }

    public void add(@NotNull final ProjectDTO project) {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void add(@Nullable final String userId, @Nullable final ProjectDTO project) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            project.setUserId(userId);
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> projects) {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            repository.addAll(projects);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @NotNull
    public ProjectDTO findOneById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final ProjectDTO model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @NotNull
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final ProjectDTO model = repository.findOneByIdAndUserId(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }
    public void remove(@NotNull final ProjectDTO project) {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void remove(@Nullable final String userId, @NotNull final ProjectDTO project) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeByUserId(userId, project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeByIdAndUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void clear() {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsById(id);
        }
    }

    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsByIdAndUserId(userId, id);
        }
    }

    public long getSize() {
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSize();
        }
    }

    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        try (@NotNull final SqlSession sqlSession = getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSizeByUserId(userId);
        }
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = getSqlSession();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return project;
    }

    public void update(@NotNull ProjectDTO project) {
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

}
