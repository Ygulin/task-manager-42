package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.SessionDTO;
import ru.tsc.gulin.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void logout(@Nullable SessionDTO session);

}
