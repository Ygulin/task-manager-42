package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IUserOwnedService;
import ru.tsc.gulin.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
