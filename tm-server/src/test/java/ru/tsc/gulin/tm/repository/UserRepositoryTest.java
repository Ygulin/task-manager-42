package ru.tsc.gulin.tm.repository;

public class UserRepositoryTest {

    /* @NotNull
    private final UserRepository userRepository = new UserRepository();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        @NotNull final User firstUser = new User();
        firstUser.setLogin("user_1");
        firstUser.setEmail("email_1@email.com");
        firstUser.setRole(Role.USUAL);
        userRepository.add(firstUser);
        @NotNull final User secondUser = new User();
        secondUser.setLogin("user_2");
        secondUser.setEmail("email_2@email.com");
        secondUser.setRole(Role.ADMIN);
        userRepository.add(secondUser);
        INITIAL_SIZE = userRepository.getSize();
    }

    @Test
    public void add() {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setEmail("email@email.com");
        user.setRole(Role.USUAL);
        userRepository.add(user);
        Assert.assertTrue(userRepository.findAll().contains(user));
        Assert.assertEquals(INITIAL_SIZE + 1, userRepository.getSize());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String userLogin = "login";
        @NotNull final Integer index = 2;
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findOneByIndex(index));
        Assert.assertEquals(userLogin, userRepository.findOneByIndex(index).getLogin());
    }

    @Test
    public void findOneById() {
        @NotNull final String userLogin = "login";
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        @NotNull final String id = user.getId();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findOneById(id));
        Assert.assertEquals(userLogin, userRepository.findOneById(id).getLogin());
        Assert.assertNull(userRepository.findOneById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(INITIAL_SIZE, users.size());
    }

    @Test
    public void remove() {
        @NotNull final User user = new User();
        userRepository.add(user);
        userRepository.remove(user);
        Assert.assertFalse(userRepository.findAll().contains(user));
        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
    }

    @Test
    public void removeByIndex() {
        @NotNull final User user = new User();
        userRepository.add(user);
        @NotNull Integer index = 2;
        userRepository.removeByIndex(index);
        Assert.assertFalse(userRepository.findAll().contains(user));
        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
    }

    @Test
    public void removeById() {
        @NotNull final User user = new User();
        @NotNull final String id = user.getId();
        userRepository.add(user);
        userRepository.removeById(id);
        Assert.assertFalse(userRepository.findAll().contains(user));
        Assert.assertEquals(INITIAL_SIZE, userRepository.getSize());
    }

    @Test
    public void clear() {
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getSize());
    }

    @Test
    public void existsById() {
        @NotNull final User user = new User();
        @NotNull final String id = user.getId();
        userRepository.add(user);
        Assert.assertTrue(userRepository.existsById(id));
    }

    @Test
    public void findOneByLogin() {
        @NotNull String userLogin = "user login";
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findOneByLogin(userLogin));
        Assert.assertEquals(userLogin, userRepository.findOneByLogin(userLogin).getLogin());
        Assert.assertNull(userRepository.findOneByLogin("new login"));
    }

    @Test
    public void findOneByEmail() {
        @NotNull String userEmail = "email@email.test";
        @NotNull final User user = new User();
        user.setEmail(userEmail);
        userRepository.add(user);
        Assert.assertNotNull(userRepository.findOneByEmail(userEmail));
        Assert.assertEquals(userEmail, userRepository.findOneByEmail(userEmail).getEmail());
        Assert.assertNull(userRepository.findOneByEmail("new email"));
    }

    @Test
    public void removeByLogin() {
        @NotNull String userLogin = "user login";
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        userRepository.add(user);
        userRepository.removeByLogin(userLogin);
        Assert.assertNull(userRepository.findOneByLogin(userLogin));
        Assert.assertEquals(INITIAL_SIZE, userRepository.findAll().size());
    } */

}
