package ru.tsc.gulin.tm.service;

public class TaskServiceTest {

    /* @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        taskService.create(USER_ID_1, "task_1", "task_1");
        taskService.create(USER_ID_2, "task_2", "task_2");
        taskService.create(USER_ID_2, "task_3", "task_3");
        INITIAL_SIZE = taskService.getSize();
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, ""));
        @NotNull final Task task = taskService.create(USER_ID_1, "task");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getName());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID_1, ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(USER_ID_1, "task", ""));
        @NotNull final Task task = taskService.create(USER_ID_1, "task", "description");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getDescription());
    }

    @Test
    public void createWithDescriptionAndDate() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "task"));
        @NotNull final Task task = taskService.create(
                USER_ID_1,
                "name",
                "description",
                DateUtil.toDate("01.01.2021"),
                DateUtil.toDate("01.10.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
        Assert.assertNotNull(task.getDateBegin());
        Assert.assertNotNull(task.getDateEnd());
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(""));
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(INITIAL_SIZE, tasks.size());
        @NotNull final List<Task> userTasks = taskService.findAll(USER_ID_2);
        Assert.assertEquals(2, userTasks.size());
        @NotNull final List<Task> newUserTasks = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, newUserTasks.size());
    }

    @Test
    public void add() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add("", new Task()));
        @Nullable Task task = null;
        Assert.assertNull(taskService.add(USER_ID_1, task));
        task = new Task();
        taskService.add(USER_ID_1, task);
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getSize());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex("", 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(USER_ID_1, -4));
        @NotNull final String taskName = "task find by index";
        taskService.create(USER_ID_1, taskName);
        @NotNull final Task task = taskService.findOneByIndex(USER_ID_1, 1);
        Assert.assertEquals(taskName, task.getName());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(USER_ID_1, ""));
        @NotNull final String taskName = "task find by id";
        @NotNull final Task task = taskService.create(USER_ID_1, taskName);
        @NotNull final String id = task.getId();
        Assert.assertNotNull(taskService.findOneById(USER_ID_1, id));
        Assert.assertEquals(taskName, taskService.findOneById(USER_ID_1, id).getName());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove("", new Task()));
        Assert.assertNull(taskService.remove(USER_ID_1, null));
        @NotNull final Task task = taskService.create(USER_ID_1, "task remove");
        taskService.remove(USER_ID_1, task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex("", 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(USER_ID_1, -4));
        @NotNull final Integer index = 1;
        taskService.create(USER_ID_1, "task remove by index");
        taskService.removeByIndex(USER_ID_1, 1);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID_1, ""));
        @NotNull final Task task = taskService.create(USER_ID_1, "task remove");
        @NotNull final String id = task.getId();
        taskService.removeById(USER_ID_1, id);
        Assert.assertEquals(INITIAL_SIZE, taskService.getSize());
        Assert.assertNull(taskService.findOneById(id));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(""));
        taskService.clear(USER_ID_1);
        Assert.assertEquals(0, taskService.getSize(USER_ID_1));
        Assert.assertEquals(INITIAL_SIZE - 1, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, ""));
        @NotNull final Task task = taskService.create(USER_ID_1, "task exists");
        Assert.assertTrue(taskService.existsById(USER_ID_1, task.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final TaskRepository repository = new TaskRepository();
        @NotNull final TaskService service = new TaskService(repository);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(0, service.getSize());
        service.create(USER_ID_1, "task");
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals(0, service.getSize(USER_ID_2));
    }

    @Test
    public void updateByIndex() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.updateByIndex("", 0, "new", "new descr")
        );
        Assert.assertThrows(IndexIncorrectException.class,
                () -> taskService.updateByIndex(USER_ID_1, -1, "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> taskService.updateByIndex(USER_ID_1, 0, "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> taskService.updateByIndex(USER_ID_1, 0, "new", "")
        );
        @NotNull final Integer index = 1;
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final Task task = taskService.updateByIndex(USER_ID_1, 0, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.updateById("", "123", "new", "new descr")
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "", "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "123", "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> taskService.updateById(USER_ID_1, "123", "new", "")
        );
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final String id = taskService.create(USER_ID_1, "old name").getId();
        @NotNull final Task task = taskService.updateById(USER_ID_1, id, name, description);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.changeTaskStatusByIndex("", 0, Status.IN_PROGRESS)
        );
        Assert.assertThrows(IndexIncorrectException.class,
                () -> taskService.changeTaskStatusByIndex(USER_ID_1, -1, Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> taskService.changeTaskStatusByIndex(USER_ID_1, 0, Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Task task = taskService.changeTaskStatusByIndex(USER_ID_1, 0, status);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.changeTaskStatusById("", "123", Status.IN_PROGRESS)
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.changeTaskStatusById(USER_ID_1, "", Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> taskService.changeTaskStatusById(USER_ID_1, "123", Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final String id = taskService.create(USER_ID_1, "task change status").getId();
        @NotNull final Task task = taskService.changeTaskStatusById(USER_ID_1, id, status);
        Assert.assertEquals(status, task.getStatus());
    } */

}
