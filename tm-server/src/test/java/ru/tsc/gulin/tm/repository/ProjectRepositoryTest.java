package ru.tsc.gulin.tm.repository;

public class ProjectRepositoryTest {

    /* @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        projectRepository.create(USER_ID_1, "project_1", "proj_1");
        projectRepository.create(USER_ID_2, "project_2", "proj_2");
        projectRepository.create(USER_ID_2, "project_3", "proj_3");
        INITIAL_SIZE = projectRepository.getSize();
    }

    @Test
    public void create() {
        projectRepository.create(USER_ID_1, "test_1");
        Assert.assertEquals(INITIAL_SIZE + 1, projectRepository.getSize());
        projectRepository.create(USER_ID_1, "test_2", "test");
        Assert.assertEquals(INITIAL_SIZE + 2, projectRepository.getSize());
    }

    @Test
    public void add() {
        @NotNull final Project project = new Project();
        projectRepository.add(USER_ID_1, project);
        Assert.assertTrue(projectRepository.findAll(USER_ID_1).contains(project));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String projectName = "project name index";
        @NotNull final Integer index = 1;
        projectRepository.create(USER_ID_1, projectName);
        Assert.assertNotNull(projectRepository.findOneByIndex(index));
        Assert.assertEquals(projectName, projectRepository.findOneByIndex(USER_ID_1, index).getName());
    }

    @Test
    public void findOneById() {
        @NotNull final String projectName = "project name id";
        @NotNull final Project project = projectRepository.create(USER_ID_1, "project name id");
        Assert.assertTrue(projectRepository.findAll().contains(project));
        @NotNull final String id = project.getId();
        Assert.assertNotNull(projectRepository.findOneById(USER_ID_1, id));
        Assert.assertEquals(projectName, projectRepository.findOneById(USER_ID_1, id).getName());
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(INITIAL_SIZE, projects.size());
        @NotNull final List<Project> projectsUser1 = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(1, projectsUser1.size());
        @NotNull final List<Project> projectsUser3 = projectRepository.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, projectsUser3.size());
    }

    @Test
    public void remove() {
        @NotNull final Project project = projectRepository.create(USER_ID_1, "project test remove");
        projectRepository.remove(USER_ID_1, project);
        Assert.assertFalse(projectRepository.findAll().contains(project));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

    @Test
    public void removeByIndex() {
        @NotNull final Project project = projectRepository.create(USER_ID_1, "project test remove");
        projectRepository.removeByIndex(USER_ID_1, 1);
        Assert.assertFalse(projectRepository.findAll().contains(project));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

    @Test
    public void removeById() {
        @NotNull final Project project = projectRepository.create(USER_ID_1, "project test remove");
        @NotNull final String id = project.getId();
        projectRepository.removeById(USER_ID_1, id);
        Assert.assertFalse(projectRepository.findAll().contains(project));
        Assert.assertEquals(INITIAL_SIZE, projectRepository.getSize());
    }

    @Test
    public void clear() {
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(2, projectRepository.getSize());
        projectRepository.clear();
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void existsById() {
        @NotNull Project project = projectRepository.create(USER_ID_1, "project test exists");
        @NotNull String id = project.getId();
        Assert.assertTrue(projectRepository.existsById(USER_ID_1, id));
    }

    @Test
    public void getSize() {
        @NotNull final ProjectRepository repository = new ProjectRepository();
        Assert.assertEquals(0, repository.getSize());
        repository.create(USER_ID_1, "project test");
        Assert.assertEquals(1, repository.getSize());
    } */

}
