package ru.tsc.gulin.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.exception.AbstractException;

public class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command `" + command + "` not supported...");
    }

}
