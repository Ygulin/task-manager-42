package ru.tsc.gulin.tm.exception.field;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
        super();
    }

    public AbstractFieldException(@NotNull final String message) {
        super(message);
    }

    public AbstractFieldException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(@NotNull Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
