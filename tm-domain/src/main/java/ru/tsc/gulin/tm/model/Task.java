package ru.tsc.gulin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Task(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final String description,
            @NotNull final Date dateBegin
    ) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.dateBegin = dateBegin;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " +
                getStatus().getDisplayName() + " : " + description;
    }

}
